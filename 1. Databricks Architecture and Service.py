# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ### Databricks Architecture
# MAGIC 
# MAGIC #### 1. Structure
# MAGIC 
# MAGIC <img src="files/tables/sample/img/db_architech.png" alt="drawing" width="800" height="1600"/>
# MAGIC 
# MAGIC #### 2. Structure - Control Plane
# MAGIC - It contains web app, repo, notebooks, metadata that databricks manages at its own cloud account (just like the cloud service AWS, Azure, etc)
# MAGIC - Majority of User's data is not located at control panel. 
# MAGIC 
# MAGIC #### 3. Structure - Data Plane 
# MAGIC - It is where all user's data located
# MAGIC - All user's computation resources are resides in data plane
# MAGIC - `Cluster` are connecting to `DBFS` and `External Datasource (Storage Account)`
# MAGIC 
# MAGIC #### 4. Control Plane - Web Application 
# MAGIC - It provides three services, which are `databricks sql`, `databricks ML`, `data science and engineering workspace`.
# MAGIC 
# MAGIC #### 5. Data Plane - Cluster 
# MAGIC - It is a computation resources that you run various task (analytics, ML, SQL, etc)
# MAGIC - `Cluster` is located at `Data Plane` and `cluster management` is located at `control plane`
# MAGIC - **Cluster are made up of one or more VM instance**
# MAGIC   - **Driver**: Coordinates activities of executors
# MAGIC   - **Executor**: Run tasks composing a Spark job
# MAGIC   - <img src="files/tables/sample/img/cluster_structure.png" alt="drawing" width="400" height="800"/> 
# MAGIC - **All Purpose Clusters vs Job Clusters**
# MAGIC   - **All Purpose Clusters**: 
# MAGIC     - Used for data analysis
# MAGIC     - Can create via workspace or CLI 
# MAGIC     - Can have up to 70 clusters for up to 30 days
# MAGIC   - **Job Clusters**: 
# MAGIC     - Used for automated jobs
# MAGIC     - the job scheduler creates job cluster when running jobs
# MAGIC     - retains up to 30 clusters
# MAGIC - **Single Node Cluster vs Multi Node Cluster**
# MAGIC   - **Single Nodes**
# MAGIC     - 1 Driver + 0 Workers
# MAGIC     - it is good to do lightweight data analysis & Single Nodes ML Workloads
# MAGIC     - Run Locally
# MAGIC     - Driver acts like both master and workers with no worker nodes 
# MAGIC     - one executor per core, minus one for driver
# MAGIC     - it cannot convert to multi core cluster
# MAGIC - **Cluster Termination vs Restart vs Delete**
# MAGIC   - **Termination**
# MAGIC     - All current in use resources are deleted such as attached storage, network connection between nodes, etc
# MAGIC     - Cluster configuration is maintained
# MAGIC     - All results that need to be persisted should be saved to a permanent location
# MAGIC   - **Restart**
# MAGIC     - Useful when we need clear out cache or wish to reset compute environment
# MAGIC   - **Delete**
# MAGIC     - It will stop our cluster and remove cluster configuration
# MAGIC 
# MAGIC #### 6. Notebook 
# MAGIC - Magic Command
# MAGIC   - `%run`
# MAGIC     - `%run ../sql/sql_notebook_1`: Relative Path
# MAGIC     - `%run /Users/vaejiang19960307@gmail.com/sql/sql_notebook_1`: Absolute Path
# MAGIC - `dbutils` Module
# MAGIC - `display()` method
# MAGIC - Export Notebook as `dbc` file
# MAGIC   - Databricks Cloud File
# MAGIC   - contain zip collection of directories and notebooks in the folder
# MAGIC   - you should not modify the file locally but it is able to upload to workspace savely 

# COMMAND ----------

DA.username

# COMMAND ----------

os.listdir('/databricks/driver')

# COMMAND ----------


